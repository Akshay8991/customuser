from django.contrib import admin
from .models import Society
from django.contrib.auth.admin import UserAdmin
from django.forms import TextInput,Textarea
class SocietyAdminConf( UserAdmin):
    search_fields = ('society_user','email','name')
    list_filter = ('society_user','email','name','is_active','is_staff')
    ordering = ('-created_date',)
    list_display = ('society_user','email','name','password','is_active','is_staff')


    fieldsets = (
        (None,{'fields':('society_user','email','name','password')}),
        ('Permissions',{'fields':('is_active','is_staff')}),
        ('Personal',{'fields':('address',)}),
    )



admin.site.register(Society,SocietyAdminConf)
