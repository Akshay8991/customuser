from django.db import models
from django.utils import timezone
from django.utils.translation import gettext_lazy as _
from django.contrib.auth.models import AbstractBaseUser,PermissionsMixin,BaseUserManager



class CustomSocietyManager(BaseUserManager):
    def create_society(self,society_user,name,email,password,**other_fields):
        if not society_user:
            raise ValueError(_('You must provide Society User'))
        email = self.normalize_email(email)
        society = self.model(society_user=society_user,name=name,email=email,**other_fields)

        society.set_password(password)
        society.save()
        return society

    def create_superuser(self,society_user,name,email,password, **other_fields):
        other_fields.setdefault('is_staff',True)
        other_fields.setdefault('is_superuser',True)
        other_fields.setdefault('is_active',True)


        if other_fields.get('is_staff') is not True:
            raise ValueError(
                'Superuser must be assigned to is_staff=True.'
            )

        if other_fields.get('is_superuser') is not True:
            raise ValueError(
                'Superuser must be assigned to is_superuser=True.'
            )

        return self.create_society(society_user,name,email,password, **other_fields)
    

class Society(AbstractBaseUser,PermissionsMixin):
    society_user = models.CharField(max_length=200,unique=True)
    email = models.EmailField(_('email address'),unique=True)
    name = models.CharField(max_length=200)
    created_date = models.DateTimeField(default=timezone.now)
    address = models.TextField(_('address'),max_length=500,blank=True)
    is_staff = models.BooleanField(default=False)
    is_active = models.BooleanField(default=False)

    objects = CustomSocietyManager()
    USERNAME_FIELD = 'society_user'
    REQUIRED_FIELDS = ['email','name']


    def __str__(self):
        return self.society_user